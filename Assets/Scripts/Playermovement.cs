using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]

public class PlayerMovement : MonoBehaviour {

    [HideInInspector]
    public Power[] powers;

    public float climbSpeed = 1;
    public Power activePower;
    public GameObject gun;
    public AudioSource jumpSource;
    public int hp = 10;
    public int mp = 10;

    internal GameObject powerHolder1;
    internal GameObject powerHolder2;
    internal GameObject powerHolder3;
    internal int score = 0;
    internal int playerNumber = 0;
    internal Animator anim;

    internal bool transition = false;
    internal bool cameraStraight = true;
    public bool cameraTurned = false;

    //item variables
    internal bool usingItem = false;

    //climbing variables
    internal bool climbing = false;
    internal GameObject ladder;

    //movement variables
    internal CharacterController controller;
    internal Vector3 startDepth;
    public float speed = 5.5f;
    internal Vector3 velocity = Vector3.zero;

    //jump variables
    internal bool canJump = true;
    internal bool jumping = false;
    Vector3 gravity = Vector3.zero;
    RaycastHit hit;
    int[] numberArray = new int[15];
    bool falling = false;
    float timer;
    float colliderCenterY;
    float colliderHeight;

    //damage variables
    bool canTakeDmg = true;


    Color baseColor;
    int powerCounter = 0;

	public virtual void Start () 
    {

        colliderCenterY = GetComponent<CharacterController>().center.y;
        colliderHeight = GetComponent<CharacterController>().height;

        timer = 0;
        startDepth = transform.position;
        if (GetComponent<Animator>())
        {
            anim = GetComponent<Animator>();
        }
        else
        {
            anim = transform.FindChild("Sphere").GetComponent<Animator>();
        }
		controller = GetComponent<CharacterController>();
        
        anim.SetBool("Move", true);
        
        activePower = powers[powerCounter];
	}

    public virtual void Update()
    {

        if (climbing)
        {
            anim.SetFloat("Speed", 0);
            gravity = Vector3.zero;
            transform.Translate(((transform.up/100)*Input.GetAxis("Vertical")) * climbSpeed);
                //gravity.y = 3*Input.GetAxis("Vertical");

           if (Input.GetButtonDown("Jump") && Mathf.Abs(Input.GetAxis("Horizontal")) >= 0.1f && !transition) { gravity = Vector3.zero; StartCoroutine("Jump");  }

        }
        else
        {
        }

        if (canJump)
        {
            anim.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
        }

        if (Input.GetAxis("Horizontal") > 0.1f && !transition || Input.GetAxis("Horizontal") < -0.1f && !transition)
        {
            if (Input.GetAxis("Horizontal") > 0.1f)
            {
                transform.LookAt(transform.position + Camera.main.transform.right);
            }
            if (Input.GetAxis("Horizontal") < -0.1f)
            {
                transform.LookAt(transform.position - Camera.main.transform.right);
            }

        }

        if (jumping)
        {
            anim.SetFloat("Speed", 0);
        }


    }

	public virtual void FixedUpdate () 
    {
        canJump = controller.isGrounded;

        anim.SetBool("Move", canJump);

        #region Fixes

        #endregion
        if (!transition)
        {
            velocity = new Vector3(0, 0, Mathf.Abs(Input.GetAxis("Horizontal") * speed));
        }
        velocity.Normalize();
        velocity = transform.TransformDirection(velocity);

        velocity *= speed;

        if (jumping)
        {

            if (timer > 0.9f)
            {
                timer = 0;
                jumping = false;
            }
            timer += Time.deltaTime * 4;
        }

        if (Input.GetButtonDown("Jump") && canJump && !jumping && !transition)
        {
            StartCoroutine("Jump");
            timer = 0.06f;
        }

        if (Input.GetButton("Jump") && canJump && !jumping && !transition)
        {
            StartCoroutine("Jump");
        }


        if (Input.GetButtonUp("Jump"))
        {

            timer = 0;
            jumping = false;
        }

        if (!canJump)
        {
            if (!jumping && !climbing)
            {
                gravity += Physics.gravity * Time.deltaTime * 3;
            }
        }
        else if (jumping)
        {
            if (timer < 0.1f) { gravity.y = 0.04f * 180f; }
            else
            {
                gravity.y = timer * 180f;
                if (gravity.y > 3f) { gravity.y = 3f; }
            }
        }
        else
        {
            colliderCenterY = 0.9f;
            controller.center = new Vector3(0, colliderCenterY, 0);
            controller.height = colliderHeight;
            gravity = Vector3.zero;
            anim.SetBool("Jump", false);

        }
        velocity += gravity;

        if (cameraTurned) { velocity.z = 0; }
        if (cameraStraight) { velocity.x = 0; }

        controller.Move(velocity * Time.fixedDeltaTime);

	}

    public void Jump()
    {

        anim.SetBool("Move", false);
        anim.SetBool("Jump", true);
        jumpSource.Play();
        colliderCenterY = 0.72f;
        controller.center = new Vector3(0, colliderCenterY, 0);
        controller.height = 1.35f;
        jumping = true;
    }

    public IEnumerator DamageCounter()
    {
        if (canTakeDmg)
        {
            canTakeDmg = false;
            yield return new WaitForSeconds(0.2f);
            transition = false;
            yield return new WaitForSeconds(0.1f);
            canTakeDmg = true;
        }
    }
    
    public IEnumerator Death()
    {
        //spawn particle system
        //emit 10 or so ominidirectional with huge particles
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 0;
        Debug.Log(gameObject.name);
        gameObject.SetActive(false);
        Time.timeScale = 1f;
        Application.LoadLevel(Application.loadedLevel);
    }

    public void TakeDamage(int dmg)
    {
        if (!canTakeDmg) { return; }
        hp -= dmg;
        anim.SetTrigger("Damaged");
        transition = true;
        float pushPower = 2.0F;
        Vector3 pushDir = new Vector3(0,0, -transform.forward.z / 2);
        if (GetComponent<PlayerMovement>())
            GetComponent<PlayerMovement>().controller.Move(pushDir * pushPower);

        if (hp < 1) { StartCoroutine("Death"); }
        else { Camera.main.GetComponentInParent<LevelCamera>().RemoveHP(); StartCoroutine("ChangeColor"); }
        StartCoroutine("DamageCounter");

    }

    public IEnumerator ChangeColor()
    {
        Camera.main.GetComponentInParent<LevelCamera>().hpBar.color = Color.red;
        yield return new WaitForSeconds(0.15f);
        Camera.main.GetComponentInParent<LevelCamera>().hpBar.color = Color.white;
        yield return null;
    }

    public bool UseMP(int amountUsed)
    {
        if (amountUsed == 0) { return true;}
        if (mp < 0) { Debug.Log("NoMana"); mp = 0; return false; }

        mp -= amountUsed;
        if (mp < 0) { Debug.Log("NoMana"); mp = 0; return false; }
        Camera.main.GetComponentInParent<LevelCamera>().RemoveMP();
        return true;
    }
    
    public void UseItem(ItemInfo itemToUse)
    {
        if (usingItem) { return; }

        switch (itemToUse.itemType)
        {
            case ItemType.hp:
                usingItem = true;
                GainHP(itemToUse.gainAmount);
                break;
            case ItemType.mp:
                usingItem = true;
                GainMP(itemToUse.gainAmount);
                break;
            case ItemType.score:
                usingItem = true;
                GainScore(itemToUse.gainAmount);
                break;
        }
    }

    private void GainScore(int p)
    {
        if (!usingItem) { return; }
        Camera.main.GetComponentInParent<LevelCamera>().score += p;
        //Camera.main.GetComponentInParent<LevelCamera>().scoreText.text = "" + score;

        usingItem = false;

    }

    private void GainMP(int p)
    {
        if (!usingItem) { return; }
        mp += p;
        if (mp > 10) { mp = 10; }
        Camera.main.GetComponentInParent<LevelCamera>().AddMP();
        usingItem = false;

    }

    private void GainHP(int p)
    {
        hp += p;
        if (hp > 10) { hp = 10; }
        Camera.main.GetComponentInParent<LevelCamera>().AddHP();
        usingItem = false;

    }
}