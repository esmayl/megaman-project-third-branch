﻿using UnityEngine;
using System.Collections;

public class IceSpike : Bullet {

    public AudioClip explosionClip;

    float iceStartDistance = 1.3f;
    float iceSpacing = 0.25f;
    float iceAmount = 10f;
    float iceDistance = 1f;
    Vector3 iceDirection;
    Vector3 icePos;
    RaycastHit rayHit;
    Rigidbody rb;

    bool hit = false;

    float timer = 0;
    float counter = 0;


    void Start()
    {
        StartCoroutine("DeathTimer");
    }
    public override void FixedUpdate()
    {
        timer = 0.1f;

        //make rotate towards ground , not working with lookAt
        if (counter > timer)
        {
            Debug.Log(transform.rotation.eulerAngles.x);

            counter = 0;
        }
        else
        {
            counter += Time.fixedDeltaTime;
        }
    }

    public override void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Ground" && !hit)
        {
            DoExplosion(coll.contacts[0].point);
            hit = true;
            return;
        }
        if (coll.gameObject.tag == "Enemy" && !hit)
        {
            if (shotSource)
            {
                shotSource.Stop();
            }
            DoExplosion(coll.contacts[0].point);
            hit = true;
            return;
        }
    }

    public void OnTriggerEnter(Collider coll)
    {

        if (coll.tag == "Enemy")
        {
            coll.gameObject.SendMessage("TakeDamage", baseDamage * weaponDamage);
            StartCoroutine("DeathTimer");
        }
        if (coll.tag == "Ground")
        {
            StartCoroutine("DeathTimer");
        }
    }

    public void DoExplosion(Vector3 center)
    {
        if (shotSource)
        {
            shotSource.Stop();
        }
        if (bulletSound && explosionClip)
        {
            bulletSound.clip = explosionClip;
            bulletSound.Play();
        }

        GameObject explosion = Instantiate(particle).gameObject;
        explosion.transform.position = center;

        Destroy(gameObject);
    }

    void InstantiateIce(Vector3 direction, Vector3 position, Vector3 scale, float min, float max)
    {
        GameObject obj = Instantiate(transform.gameObject) as GameObject;

        ShapeIce(obj, scale);
        obj.transform.position = position;
        obj.transform.Rotate(transform.forward, Random.Range(min, max));
        if (direction.z > 0.1f)
        {
            obj.transform.Rotate(transform.right, Random.Range(0, -90));
        }
        else
        {
            obj.transform.Rotate(transform.right, Random.Range(-180, -90));
        }
        obj.transform.GetComponent<Collider>().isTrigger = true;
        Destroy(obj.GetComponent<Rigidbody>());
    }

    public void ShapeIce(GameObject ga, Vector3 scale)
    {
        ga.SendMessage("ShapeIce", scale);

    }
}
