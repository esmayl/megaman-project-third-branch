﻿using UnityEngine;
using System.Collections;

public enum BossStates { Moving,MovingToPlayer,Attack1,Attack2,Attack3,Jump}

public class BossBase : MonoBehaviour {


    internal RaycastHit hit;
    internal GameObject player;
    internal BossStates currentState;
    internal Vector3 walkDirection;
    internal Vector3 velocity;
    internal Rigidbody controller;

    public GameObject gun;
    public GameObject bullet;
    public float speed = 5;
    public int meleeDamage = 5;
    public int range = 10;
    public float rangeToStop = 1f;
    public int height = 3;
    public float hp = 100;

    internal Vector3[] pos;
    internal int attack1Counter = 0;
    internal int attack2Counter = 0;
    internal int attack3Counter = 0;

	public virtual void Start () {
        controller = GetComponent<Rigidbody>();
        StartCoroutine("DetectPlayer", range);
	}
	
	public void Update () {
        if (player)
        {
            gun.transform.LookAt(player.transform.position + transform.up);
        }
	}

    public virtual IEnumerator DetectPlayer(float Radius)
    {
        while (true)
        {

            //==============================UPDATE========================================//

            Collider[] hits;
            hits = Physics.OverlapSphere(transform.position, range);
            if (hits.Length > 0)
            {
                foreach (Collider h in hits)
                {
                    if (h.gameObject.layer == LayerMask.NameToLayer("Player"))
                    {
                        player = h.gameObject;
                        Vector3 playerPos = player.transform.position;
                        playerPos.y = transform.position.y;
                        if (Mathf.Abs(Vector3.Distance(player.transform.position, transform.position)) < range && Mathf.Abs(Vector3.Distance(player.transform.position, transform.position)) > range / 1.5f)
                        {
                            currentState = BossStates.MovingToPlayer;
                            transform.LookAt(player.transform.position + transform.up);
                        }
                        if (Mathf.Abs(Vector3.Distance(player.transform.position, transform.position)) < range / 1.5f && Mathf.Abs(Vector3.Distance(player.transform.position, transform.position))> range / 3)
                        {
                            if (attack1Counter < 2)
                            {
                                transform.LookAt(player.transform.position + transform.up);
                                currentState = BossStates.Attack1;

                                if (attack2Counter >= 1)
                                {
                                    attack2Counter=0;
                                }
                            }
                            else if (attack2Counter < 1)
                            {
                                transform.LookAt(player.transform.position + transform.up);
                                currentState = BossStates.Attack2;

                                if (attack1Counter >= 2)
                                {
                                    attack1Counter=0;
                                }
                            }
                        }
                        
                        if (Mathf.Abs(Vector3.Distance(player.transform.position, transform.position)) <= range / 3)
                        {
                            currentState = BossStates.Attack2;
                        }
                        if (player)
                        {
                            //using sphere cast to fake damage on collision
                            if (Mathf.Abs(Vector3.Distance(player.transform.position, transform.position)) < rangeToStop)
                            {
                                player.gameObject.GetComponent<PlayerMovement>().TakeDamage(meleeDamage);
                            }
                        }
                        hits = null;
                       
                    }
                }
            }
            switch (currentState)
            {
                case BossStates.Moving:
                    break;
                case BossStates.Jump:
                    Jump();
                    break;
                case BossStates.Attack1:
                    Attack1();
                    break;
                case BossStates.Attack2:
                    Attack2();
                    MoveRandomDirection();
                    break;
                case BossStates.Attack3:
                    break;
                case BossStates.MovingToPlayer:
                    MoveToPlayer(player.transform);
                    break;

                default:
                    break;
            }

            


            yield return new WaitForSeconds(0.3f);
        }
    }

    public void Attack1()
    {
        attack1Counter++;

        
        //using transform.up to make sure the bullet instances above the ground
        GameObject tempObj = Instantiate(bullet, gun.transform.position+transform.forward, Quaternion.identity) as GameObject;
        tempObj.transform.LookAt(gun.transform.position+gun.transform.forward*2);
        tempObj.GetComponent<Bullet>().bulletSound = player.transform.FindChild("AttackSource 3").GetComponent<AudioSource>();
        //tempObj.GetComponent<SphereCollider>().isTrigger = true;
    }
    public virtual void Attack2(){}
    

    public void MoveRandomDirection()
    {
        Vector3[] walkDirs = new Vector3[] {gun.transform.forward,-gun.transform.forward};
        controller.velocity = ((walkDirs[Random.Range(0, walkDirs.Length - 1)].normalized * speed));
    }


    public void MoveToPlayer(Transform target)
    {
        walkDirection = target.position - transform.position;
        //floating enemy?
        //walkDirection.y = 0;
        velocity = controller.velocity;

        if (walkDirection.magnitude > rangeToStop)
        {
            velocity = walkDirection.normalized * speed;
        }

        controller.velocity =velocity;
    }

    public void Jump()
    {
        velocity = GetComponent<Rigidbody>().velocity;
        velocity.y += height;
        controller.AddForce(velocity, ForceMode.Impulse);
    }

    public void TakeDamage(float damage)
    {
        if (hp < 0)
        {
            ItemDatabase.DropItem(transform.position, transform.name);
            Destroy(gameObject);
        }

        hp -= damage;
        if (hp <= 0)
        {
            ItemDatabase.DropItem(transform.position, transform.name);
            Destroy(gameObject);
        }
    }
}
