using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public AudioSource shotSource;
    public AudioSource bulletSound;
    public GameObject particle;
    internal float baseDamage=1f;
    public float weaponDamage = 1f;
    public float lifeTime = 5;
    public int bulletSpeed = 5;

    internal Rigidbody controller;


    void Start()
    {
        controller = GetComponent<Rigidbody>();
        particle.SetActive(false);
        StartCoroutine("DeathTimer");
    }

    public virtual void FixedUpdate()
    {
        controller.velocity = transform.forward * bulletSpeed;
    }

    void Update()
    {
        transform.Rotate(transform.forward, 1f);
    }

    public virtual void OnCollisionEnter(Collision coll)
    {
        gameObject.GetComponent<Collider>().enabled = false;

        switch (coll.gameObject.tag)
        {
            case "Shield":
                if (gameObject.layer != LayerMask.NameToLayer("EnemyProjectiles"))
                {
                    Debug.Log("Hit shield");
                    GetComponent<Rigidbody>().velocity = Vector3.zero;
                }
                return;
            case "Enemy":
                if (gameObject.layer != LayerMask.NameToLayer("EnemyProjectiles"))
                {
                    Debug.Log("Hit enemy");

                    coll.gameObject.SendMessage("TakeDamage", baseDamage * weaponDamage);
                    StartCoroutine("Hit", coll.contacts[0].point);
                }
                return;
            case "Ground":
                StartCoroutine("Hit", coll.contacts[0].point);
                return;
        }

       if (gameObject.layer == LayerMask.NameToLayer("EnemyProjectiles"))
       {
            if (coll.gameObject.tag == "Player")
            {
                coll.gameObject.SendMessage("TakeDamage", baseDamage * weaponDamage);
                StartCoroutine("Hit", coll.contacts[0].point);
            }
       }

    }

    public IEnumerator Hit(Vector3 particlePos)
    {
        particle.transform.position = particlePos;
        particle.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        particle.GetComponent<Renderer>().enabled = false;
        Destroy(gameObject);
    }
    public virtual IEnumerator DeathTimer()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }
}
