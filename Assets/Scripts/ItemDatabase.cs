﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum ItemType
{
    hp,mp,score
}


public class ItemDatabase : MonoBehaviour {

    public static Dictionary<string, ItemInfo> lootTable = new Dictionary<string,ItemInfo>();

    [SerializeField]
    public Dictionary<string, ItemInfo> publicLootTable = new Dictionary<string, ItemInfo>(3);
    ItemInfo loot = new ItemInfo();
    ItemInfo loot2 = new ItemInfo();
    ItemInfo loot3 = new ItemInfo();

	void Start () {

        loot.itemName = "Health Potion";
        loot.gainAmount = 2;
        loot.itemType = ItemType.hp;
        loot.meshMaterial = new Material(Shader.Find("Standard"));

        loot2.itemName = "Score";
        loot2.gainAmount = 20;
        loot2.itemType = ItemType.score;
        loot2.meshMaterial = new Material(Shader.Find("Standard"));

        loot3.itemName = "Mana Potion";
        loot3.gainAmount = 5;
        loot3.itemType = ItemType.mp;
        loot3.meshMaterial = new Material(Shader.Find("Standard"));

        ItemDatabase.AddToList("MeleeEnemy", loot);
        ItemDatabase.AddToList("Turret", loot2);
        ItemDatabase.AddToList("RangedEnemy", loot3);
        ItemDatabase.AddToList("MiniBoss", loot);
	}

    public static void AddToList(string enemyName, ItemInfo item)
    {
        if (lootTable.ContainsKey(enemyName))
        {
            Debug.Log(enemyName);
            lootTable[enemyName] = item;
            return;
        }
        lootTable.Add(enemyName, item);
    }
    public static void DropItem(Vector3 deathPosition, string name)
    {
        GameObject loot = GameObject.CreatePrimitive(PrimitiveType.Cube);
        loot.transform.position = deathPosition;
        loot.name = lootTable[name].itemName;

        loot.AddComponent<ItemFunctions>().item.gainAmount = lootTable[name].gainAmount;
        loot.GetComponent<ItemFunctions>().item.itemName = lootTable[name].itemName;
        loot.GetComponent<ItemFunctions>().item.itemType = lootTable[name].itemType;
        loot.GetComponent<ItemFunctions>().item.meshMaterial = lootTable[name].meshMaterial;
        loot.GetComponent<ItemFunctions>().item.mesh = loot;
        loot.GetComponent<Collider>().isTrigger = true;
        
    }
}
