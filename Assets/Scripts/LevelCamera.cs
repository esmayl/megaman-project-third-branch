﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelCamera : MonoBehaviour {

    public GameObject player;
    public Image hpBar;
    public Text hptext;
    public Image chargeBar;
    public Text scoreText;
    public float moveSpeed;
    public float cameraDistance;
    public Vector3 startPos;

    internal int score = 0;
    Image[] hp = new Image[10];
    Image[] mp = new Image[10];
    RaycastHit hit;

    void Start()
    {
        if (hpBar)
        {
            hpBar.fillAmount = 1;
        }
        if (hptext)
        {
            hptext.text = player.GetComponent<PlayerMovement>().hp * 10 + "%";
        }
        scoreText.text = score.ToString("000000000");

    }

    public void RemoveHP()
    {
        if (player.GetComponent<PlayerMovement>().hp > 0)
        {
            if (hptext)
            {
                hptext.text = player.GetComponent<PlayerMovement>().hp * 10 + "%";
            }
            hpBar.fillAmount = player.GetComponent<PlayerMovement>().hp / 10f;
        }
    }

    public void AddHP()
    {
        if (player.GetComponent<PlayerMovement>().hp > 0)
        {
            if (hptext)
            {
                hptext.text = player.GetComponent<PlayerMovement>().hp * 10 + "%";
            }
            hpBar.fillAmount = player.GetComponent<PlayerMovement>().hp / 10f;
        }
    }

    public void AddMP()
    {
        if (player.GetComponent<PlayerMovement>().mp >= 0)
        {
            for (int i = 0; i < (int)player.GetComponent<PlayerMovement>().mp; i++)
            {
                if (i >= 9) { return; }
                mp[i].enabled = true;
            }
        }
    }

    public void RemoveMP()
    {
        if (player.GetComponent<PlayerMovement>().mp >= 0)
        {
            for (int i = (int)player.GetComponent<PlayerMovement>().mp; i < 9; i++)
            {
                if (i >= 9) { return; }
                mp[i].enabled = false;
            }
        }
    }

    public void AddScore()
    {
        if (player.GetComponent<PlayerMovement>().score >= 0)
        {
            scoreText.text = "" + score.ToString("000000000");
        }
    }

    void FixedUpdate()
    {
        SafeFrameCheck();
    }

    void CameraLerpForward()
    {
        Vector3 newPos = transform.position + transform.right/4;
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * moveSpeed);
    }
    void CameraLerpBackward()
    {
        Vector3 newPos = transform.position - transform.right / 4;
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * moveSpeed);
    }
    void CameraLerpDownward()
    {
        Vector3 newPos = transform.position - transform.up / 4;
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * moveSpeed);
    }
    void CameraLerpUpward()
    {
        Vector3 newPos = transform.position + transform.up / 4;
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * moveSpeed);
    }

    public IEnumerator MoveForward()
    {
        for (int i = 0; i < 60; i++)
        {
            Vector3 screenPos = transform.GetComponent<Camera>().WorldToScreenPoint(player.transform.position);
            float ratio = screenPos.x / Screen.width;

            if (ratio < 0.6) { Time.timeScale = 1; StopAllCoroutines(); }
            else
            {
                CameraLerpForward();
            }
            yield return new WaitForEndOfFrame();
        }

    }

    public IEnumerator MoveBackward()
    {
        for (int i = 0; i < 60; i++)
        {
            Vector3 screenPos = transform.GetComponent<Camera>().WorldToScreenPoint(player.transform.position);
            float ratio = screenPos.x / Screen.width;

            if (ratio > 0.4) { Time.timeScale = 1; StopAllCoroutines(); }
            else
            {
                CameraLerpBackward();
            }
            yield return new WaitForEndOfFrame();
        }
        
    }
    public IEnumerator MoveDown()
    {
        for (int i = 0; i < 60; i++)
        {
            Vector3 screenPos = transform.GetComponent<Camera>().WorldToScreenPoint(player.transform.position);
            float ratioY = screenPos.y / Screen.height;

            if (ratioY > 0.6) { Time.timeScale = 1; StopAllCoroutines(); }
            else
            {
                CameraLerpDownward();
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public IEnumerator MoveUp()
    {
        for (int i = 0; i < 60; i++)
        {
            Vector3 screenPos = transform.GetComponent<Camera>().WorldToScreenPoint(player.transform.position);
            float ratioY = screenPos.y / Screen.height;

            if (ratioY < 0.6) { Time.timeScale = 1; StopAllCoroutines(); }
            else
            {
                CameraLerpUpward();
            }
            yield return new WaitForEndOfFrame();
        }
    }

    bool SafeFrameCheck()
    {
        Application.targetFrameRate = 60;

            Vector3 screenPos = transform.GetComponent<Camera>().WorldToScreenPoint(player.transform.position);
            float ratio = screenPos.x / Screen.width;
            float ratioY = screenPos.y / Screen.height;
            if (ratioY < 0.01f)
            {
                StopAllCoroutines();
                StartCoroutine("MoveDown");
                return false;
            }
            else if (ratio < 0.4f)
            {
                StopAllCoroutines();
                StartCoroutine("MoveBackward");
                return false;
            }
           

            if (ratio > 0.6f)
            {
                StopAllCoroutines();
                StartCoroutine("MoveForward");
                return false;
            }
            else if (ratioY > 0.8f)
            {
                StopAllCoroutines();
                StartCoroutine("MoveUp");
                return false;
            }
            
            return true;

    }
}
