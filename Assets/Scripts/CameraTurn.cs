﻿using UnityEngine;
using System.Collections;

public class CameraTurn : MonoBehaviour {

    public float speed;

    public void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            if (col.GetComponent<PlayerMovement>().cameraStraight)
            {
                col.GetComponent<PlayerMovement>().cameraStraight = false;
                col.GetComponent<PlayerMovement>().cameraTurned = true;
            }
            else
            {
                col.GetComponent<PlayerMovement>().cameraStraight = true;
                col.GetComponent<PlayerMovement>().cameraTurned = false;
            }
            col.GetComponent<PlayerMovement>().transition = true;
            StartCoroutine("MoveTo",col.gameObject);
            transform.GetComponent<Collider>().enabled = false;
        }
    }

    public IEnumerator MoveTo(GameObject collider)
    {
        int degrees = 0;
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, transform.position.y, Camera.main.transform.position.z);

        while (degrees < 90)
        {
            Camera.main.transform.LookAt(transform);
            Camera.main.transform.RotateAround(transform.position, transform.up, -speed);
            degrees++;
            yield return new WaitForFixedUpdate();
        }
        collider.GetComponent<PlayerMovement>().transition = false;
        yield return null;
    }
}
